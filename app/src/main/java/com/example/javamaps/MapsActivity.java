package com.example.javamaps;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.location.LocationListener;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    EditText text;
    View mapView;
    FloatingActionButton fab, fabExpand;
    boolean isExpand = false, isAnimationPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        text=findViewById(R.id.editText);
        fab = findViewById(R.id.fab);
        mapView = mapFragment.getView();
        fabExpand = findViewById(R.id.fab_expand);

//        LinearLayout bottomSheet = findViewById(R.id.bottom_sheet);
//
//        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//        bottomSheetBehavior.setPeekHeight(80);
//        bottomSheetBehavior.setHideable(true);

        mng = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        fabExpand.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                if(isExpand){
                    if(!isAnimationPlaying){
                        isAnimationPlaying = true;
                        Animation animation = new TranslateAnimation(0, -1190, 0, 0);
                        animation.setDuration(750);
                        animation.setFillAfter(true);
                        text.startAnimation(animation);
                        fab.startAnimation(animation);

                        fab.setVisibility(View.INVISIBLE);
                        text.setVisibility(View.INVISIBLE);
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                isAnimationPlaying = false;
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        fabExpand.setImageDrawable(getResources().getDrawable(R.drawable.expand_icon, MapsActivity.this.getTheme()));

                        isExpand = !isExpand;
                    }
                }else{
                    if(!isAnimationPlaying){
                        isAnimationPlaying = true;
                        fab.setVisibility(View.VISIBLE);
                        text.setVisibility(View.VISIBLE);

                        Animation animation = new TranslateAnimation(-1200, 0, 0, 0);
                        animation.setDuration(750);
                        animation.setFillAfter(true);
                        text.startAnimation(animation);
                        fab.startAnimation(animation);
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                isAnimationPlaying = false;
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        fabExpand.setImageDrawable(getResources().getDrawable(R.drawable.collapse_icon, MapsActivity.this.getTheme()));

                        isExpand = !isExpand;
                    }
                }
            }
        });


    }
    double lat;
    double lon;
    LocationManager mng;
    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);

            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 320);

            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            Location location = mng.getLastKnownLocation(mng.getBestProvider(new Criteria(), false));
            lat = location.getLatitude();
            lon = location.getLongitude();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 12);
            mMap.animateCamera(cameraUpdate);
        }
        catch (Exception ex)
        {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Geocoder geocoder = new Geocoder(MapsActivity.this, new Locale("ru", "RU"));
                try {
                    List<Address> info = geocoder.getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude, 1);
                    Address address = info.get(0);
                    Toast.makeText(getApplicationContext(), address.getAddressLine(0), Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return true;
            }
        });

    }

    String latEnd;
    String lngEnd;
    LatLng marker;
    float distance = 0.0f;
    public void click(final View view) {
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.e("err", e.getMessage());
        }
        getPathTo();
    }

    public void getPathTo(){
        mMap.clear();
        distance = 0.0f;
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin="+lat+","+lon+"&destination="+text.getText()+"&key=AIzaSyCMWKAP0ajGU-CkLWhVA-z4IkaEnzMHzWM";
        JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray routes = response.getJSONArray("routes");
                    JSONObject index1=routes.getJSONObject(0);
                    JSONArray legs=index1.getJSONArray("legs");
                    JSONObject index2=legs.getJSONObject(0);
                    JSONArray steps=index2.getJSONArray("steps");
                    Integer count=index2.getJSONArray("steps").length();
                    for(int i=0;i<count;i++) {
                        JSONObject index3 = steps.getJSONObject(i);
                        JSONObject start_location = index3.getJSONObject("start_location");
                        String latStart = start_location.getString("lat");
                        String lngStart = start_location.getString("lng");
                        JSONObject end_location = index3.getJSONObject("end_location");
                        latEnd = end_location.getString("lat");
                        lngEnd = end_location.getString("lng");
                        float[] currentDistance = new float[1];
                        Location.distanceBetween(Double.valueOf(latStart), Double.valueOf(lngStart), Double.valueOf(latEnd), Double.valueOf(lngEnd), currentDistance);
                        distance += currentDistance[0];
                        mMap.addPolyline(new PolylineOptions()
                                .color(0xff4169e1)
                                .clickable(true)
                                .add(
                                        new LatLng(Double.valueOf(latStart), Double.valueOf(lngStart)),
                                        new LatLng(Double.valueOf(latEnd), Double.valueOf(lngEnd))));
                    }
                    marker = new LatLng(Double.valueOf(latEnd), Double.valueOf(lngEnd));
                    mMap.addMarker(new MarkerOptions()
                            .position(marker)
                            .title(new DecimalFormat("#.##").format(distance / 1000.0f) + " км"));
                } catch (JSONException e) {
                }
            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { }});
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jo);
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();
        mng.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000 * 10, 10, locationListener);
        mng.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 1000 * 10, 10,
                locationListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mng.removeUpdates(locationListener);
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            lat = location.getLatitude();
            lon = location.getLongitude();
            getPathTo();
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
}
